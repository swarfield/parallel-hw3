all: homework3 C_example

homework3: hw3.cu
	nvcc -g -G -o homework3 hw3.cu -O3 -arch=sm_35 -D_FORCE_INLINE

release: hw3.cu
	nvcc -o homework3 hw3.cu -O3 -arch=sm_35 -D_FORCE_INLINE

C_example: sparse_matvec.c
	gcc sparse_matvec.c -o C_example

.PHONY: release