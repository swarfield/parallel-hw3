#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

// A Kernal
__global__ void SpVM(int n, int* rowPtr, int* ind, float* val, float* b, float* finalT){
    int myi = blockIdx.x * blockDim.x + threadIdx.x;
    if (myi < n){
        float t = 0;
        int lb = rowPtr[myi];
        int ub = rowPtr[myi + 1];
        for (int j = lb; j < ub; j++){
            int index = ind[j];
            t += val[j] * b[index];
        }
        finalT[myi]= t;

    }
}
// Main
int main(int argc, char *argv[]){
    // Is there a proper amount of arguments
    if (argc != 2) {
        cout << "usage: " << argv[0] << " File_with_Array.txt" << endl;
        return -1;
    }

    // Vars that hold matrix size
    ifstream File_with_Array(argv[1]);
    int n; // number of nonzero elements in data
    int nr; // number of rows in matrix
    int nc; // number of columns in matrix
    string line;

    // Get rid of comments at the top of file
    while(true){
        getline(File_with_Array, line);
        if (line[0] != '%') {
            break;
        }
    }

    // Convert line to a stream, because it is nice to use
    istringstream lineToNum(line);
    lineToNum >> nr >> nc >> n;

    // Malloc CPU
    int* indices = new int[n*sizeof(int)]; // Store indices
    float* b = new float[nc*sizeof(float)];
    float* t = new float[nr*sizeof(float)];
    float* gt = new float[nr*sizeof(float)];
    float* data = new float[n*sizeof(float)];
    int* ptr = new int[(nr+1)*sizeof(int)];

    // Code taken from C example for filing memory
    int lastr=0;
    for (int i = 0; i < n; i++) {
        int r;
        File_with_Array >> r >> indices[i] >> data[i];
        indices[i]--;  // start numbering at 0
    
        if (r!=lastr) { 
            ptr[r-1] = i; 
            lastr = r; 
        }
    }
    ptr[nr] = n;

    // initialize t to 0 and b with random data  
    for (int i = 0; i < nr; i++) {
        t[i] = 0.0;
        gt[i] = 0.0;
    }

    for (int i = 0; i < nc; i++) {
        b[i] = (float) rand() / RAND_MAX;
    }

    // MAIN COMPUTATION, SEQUENTIAL VERSION
    cout << "Sequential Solution" << endl;
    for (int i = 0; i < nr; i++) {                                                      
        for (int j = ptr[i]; j < ptr[i+1]; j++) {
            t[i] = t[i] + data[j] * b[indices[j]];
        }

        cout << t[i] << ' ';
    }
    cout << endl;
    
    // Malloc GPU
    float* gpuB;
    float* gpuT;
    float* gpuData;
    int* gpuRowPtr;
    int* gpuIndices;

    cudaMalloc(&gpuB, nc*sizeof(float));
    cudaMalloc(&gpuT, nr*sizeof(float));
    cudaMalloc(&gpuData, n*sizeof(float));
    cudaMalloc(&gpuRowPtr, (nr+1)*sizeof(int));
    cudaMalloc(&gpuIndices, n*sizeof(int));

    // Memcopy
    cudaMemcpy(gpuB, b, nc*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(gpuT, gt, nr*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(gpuData, data, n*sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(gpuRowPtr, ptr, (nr+1)*sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpuIndices, indices, n*sizeof(int), cudaMemcpyHostToDevice);


    // Setup blocks
    dim3 numThreads(16,1,1);
    dim3 numBlocks(1 + n / numThreads.x, 1 ,1);

    // Call Kernal
    SpVM<<<numBlocks, numThreads>>>(n, gpuRowPtr, gpuIndices, gpuData, gpuB, gpuT);
    
    // Copy answer back
    cudaMemcpy(t, gpuT, nr*sizeof(float),cudaMemcpyDeviceToHost);

    cout << "Parallel Soln." << endl;
    for (int i = 0; i < nr; i++){
        cout << t[i] << ' ';
    }
    cout << endl;

}
